@extends('layout.master')
@section('title','Buat Account Baru!')
@section('content')

        <h3>Sign Up Form</h3>
        <form action="kirim">
            <label>First name:</label><br>
            <input type="text" id="fname" name="fname"><br><br>
            <label>Last name:</label><br>
            <input type="text" id="lname" name="lname"><br><br>

            <label>Gender</label><br>
            <input type="radio" id="male" name="gender" value="male">
            <label>Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label>Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label>Other</label><br><br>

            <label>Nationality:</label><br>
            <select>
                <optgroup>
                    <option>Indonesia</option>
                    <option>Inggris</option>
                </optgroup>
            </select><br><br>

            <label>Language Spoken:</label><br>
            <input type="checkbox" id="indonesia" name="indonesia" value="indonesia">
            <label> Bahasa Indonesia</label><br>
            <input type="checkbox" id="inggris" name="inggris" value="inggris">
            <label> Inggris</label><br>
            <input type="checkbox" id="Other" name="Other" value="Other">
            <label> Other</label><br><br>

            <label>Bio:</label><br>
            <textarea cols="30" rows="5" id="bio"></textarea><br>
            <input type="Submit" value="Sign Up">
        </form>

        @endsection