<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class democontroller extends Controller
{
    public function index(){
    	return view('welcome');
    }
    public function form(){
    	return view('form');
    }
    public function kirim(Request $request){
    	$name = $request->nama;
    	$gender = $request->jenkel;
    	$national = $request->nasional;

    	return view('kirim', compact('name','gender','national'));
    }
    public function welcome()
    {
    	return view('index');
    }
    public function test()
    {
    	return view('test');
    }
}
