<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','democontroller@index');
Route::get('form','democontroller@form');
Route::get('kirim','democontroller@kirim');
Route::get('welcome','democontroller@welcome');
Route::get('test','democontroller@test');